#!/bin/bash

main() {
    STOP=0
    START=0
    BACKUP=0
    RESTORE=0
    HELP=0
    TAG=""

    #
    # Process script arguments
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        stop)
            STOP=1
            ;;
        start)
            START=1
            ;;
        restart)
            STOP=1
            START=1
            ;;
        backup)
            BACKUP=1
            ;;
        restore)
            RESTORE=1
            ;;
        -h|--help)
            HELP=1
            ;;
        *)
            echo "Unkown option: $key"
            HELP=1
            ;;
    esac
    shift # past argument or value
    done

    #
    # Execute based on mode argument
    #
    if [ ${HELP} -eq 1 ]; then
        echo ""
        echo "control.sh [start|stop|restart] [-h]"
        echo ""
        echo "  start                 Start Proxy"
        echo "  stop                  Stop Proxy"
        echo "  restart               Restart Proxy"
        echo "  backup                Create a backup"
        echo "  restore               Restore a backup"
        echo ""
        echo "  -h, --help    Show help"
        echo ""
        exit 0
    else
        COMPOSE_OPTS=""
        COMPOSE_DIR="clarin"
        if [ $(readlink $0) ]; then
            PROJECT_DIR=$(dirname $(readlink $0))
        else
            PROJECT_DIR=$(dirname $BASH_SOURCE)
        fi
        COMPOSE_DIR=$PROJECT_DIR/$COMPOSE_DIR

        if [ ${STOP} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml down $COMPOSE_OPTS)
        fi

        if [ "${BACKUP}" -eq 1 ]; then
            echo "Backup not supported"
        fi

        if [ "${RESTORE}" -eq 1 ]; then
            echo "Backup not supported"
        fi

        if [ ${START} -eq 1 ]; then

            result=$(docker image ls | grep clavas | grep 20170125 | wc -l)
            if [ "${result}" == "0" ]; then
                (
                    echo "Downloading CLAVAS docker image" && \
                    cd /tmp && \
                    curl -o clavas-20170125.tar.bz2 https://b2drop.eudat.eu/s/w4hQcFYQQgzE1tw/download && \
                    echo "Decompressing CLAVAS docker image" && \
                    bunzip2 clavas-20170125.tar.bz2 && \
                    echo "Importing CLAVAS docker image" && \
                    docker load -i clavas-20170125.tar && \
                    rm clavas-20170125.tar
                )
            fi

            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml up -d)
        fi
    fi
}
main "$@"; exit